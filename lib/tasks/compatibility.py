from invoke import Collection, task

from src.compatibility_service import CompatibilityService
from src.project_repository import ProjectRepository
from src.readme import ReadMe


@task(default=True)
def default(context):
    """Regenerate the README.md with the updated compatibility table."""
    projects = ProjectRepository().load_all()
    compatibilities = CompatibilityService().check(projects)
    ReadMe().generate(compatibilities.generate_markdown_table())
    print('README.md has been successfully updated.')


compatibility = Collection(default=default)
