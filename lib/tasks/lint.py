from invoke import Collection, run, task


@task
def python(context):
    """Lint Python code."""
    run('find . -name "*.py" -print0 | xargs -0 flake8 --show-source --config=./.flake8')
    print('Python lint found no errors.')


lint = Collection(python)
