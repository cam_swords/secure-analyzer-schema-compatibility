
class CompatibilityResult:

    def __init__(self, project, report_version_schema, report_version_result, closest_compatible_result, latest_schema_result):
        self.project = project
        self.report_version_schema = report_version_schema
        self.report_version_result = report_version_result
        self.closest_compatible_result = closest_compatible_result
        self.latest_schema_result = latest_schema_result

    def comparable(self):
        return self.project.name

    def generate_markdown_table(self):
        report_results = self.summarize_report_version()
        upgrade_path = self.summarize_upgrade_path()

        return f'| {self.project_link()}<br/>{self.project.report_type()} | {report_results} | {upgrade_path} |'

    def project_link(self):
        if self.project.url:
            return f'<a name="{self.project.name}" href="{self.project.url}">{self.project.name}</a>'

        return f'<a name="{self.project.name}">{self.project.name}</a>'

    def summarize_upgrade_path(self):
        if self.latest_schema_result.succeeded() and \
           self.report_version_schema == self.latest_schema_result.schema_version():
            return 'Up-to date! 😎'

        messages = []

        if self.closest_compatible_result.succeeded():
            messages.append(f'Compatible to {self.schema_version_link(self.closest_compatible_result)} 🙂<br/><br/>')

        if not self.latest_schema_result.succeeded():
            messages.append(f'To upgrade to {self.schema_version_link(self.latest_schema_result)}:')
            messages.append(f'{self.errors_list(self.latest_schema_result)}')

        return ''.join(messages)

    def schema_version_link(self, result):
        return f'[{result.schema_version()}]({result.schema_url()})'

    def summarize_report_version(self):
        report_link = f'[{self.report_version_schema}]({self.project.example_report})'

        if self.report_version_result and self.report_version_result.succeeded():
            return f'Compatible {report_link} 🙂'
        elif not self.report_version_result:
            return f'Not compatible, {report_link} is not a schema version 🙁'
        else:
            return f'Not compatible {report_link} 🙁'\
                   f'{self.errors_list(self.report_version_result)}'

    def errors_list(self, result):
        list_of_errors = [f'<li>{error}</li>' for error in result.summarized_errors()]
        return f'<ul>{"".join(list_of_errors)}</ul>'
