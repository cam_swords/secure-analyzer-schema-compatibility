from datetime import datetime


class CompatibilityResults:

    def __init__(self, compatibility_results):
        self.compatibility_results = compatibility_results

    def __len__(self):
        return len(self.compatibility_results)

    def generate_markdown_table(self):
        time = datetime.today().strftime('%d %b %Y at %I:%M %p')

        table = [f'Last updated: {time}.\n',
                 '| Project/Type | Current Version | Upgrade path |',
                 '| ------------ | --------------- | ------------ |']

        sorted_results = sorted(self.compatibility_results, key=lambda result: result.comparable())

        for compatibility_result in sorted_results:
            table.append(compatibility_result.generate_markdown_table())

        return '\n'.join(table)
