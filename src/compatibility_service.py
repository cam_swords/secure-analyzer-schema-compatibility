from .compatibility_result import CompatibilityResult
from .compatibility_results import CompatibilityResults
from .failed_compatibility_result import FailedCompatibilityResult
from .secure_schemas_repository import SecureSchemaRepository


class CompatibilityService:

    def check(self, projects):
        compatibilities = [self.check_project(project) for project in projects]
        return CompatibilityResults(compatibilities)

    def check_project(self, project):
        ok, error_msg = project.can_download_report()

        if not ok:
            return FailedCompatibilityResult(project, error_msg)

        schemas = SecureSchemaRepository().load_all(project.report_type())
        report_version_result = self.validate_report_version(schemas, project)
        most_compatible_result = schemas.validate(project.example_report_content()).least_errors()
        latest_schema_result = schemas.most_recent().validate(project.example_report_content())

        return CompatibilityResult(project, project.report_version(), report_version_result,
                                   most_compatible_result, latest_schema_result)

    def validate_report_version(self, schemas, project):
        report_version_schema = schemas.for_version(project.report_version())

        if not report_version_schema:
            return None

        return report_version_schema.validate(project.example_report_content())
