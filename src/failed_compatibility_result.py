
class FailedCompatibilityResult:

    def __init__(self, project, error_message):
        self.project = project
        self.error_message = error_message

    def comparable(self):
        return ''

    def generate_markdown_table(self):
        report = f'[report.json]({self.project.example_report})'
        return f'| {self.project.name} | error {report} | |'
