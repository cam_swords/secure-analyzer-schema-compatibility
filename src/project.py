import json

import requests

from .schema_version import SchemaVersion


class Project:

    def __init__(self, name, url, example_report):
        self.name = name
        self.url = url
        self.example_report = example_report
        self._example_report_content = None

    def example_report_content(self):
        if not self._example_report_content:
            response = requests.get(self.example_report)
            response.raise_for_status()

            if not response.content:
                raise RuntimeError(f'No content returned for {self.example_report}')

            self._example_report_content = json.loads(response.content)

        return self._example_report_content

    def can_download_report(self):
        try:
            response = requests.get(self.example_report)
            response.raise_for_status()
            return True, ''
        except requests.HTTPError as error:
            return False, str(error)

    def report_type(self):
        report = self.example_report_content()

        if 'scan' not in report or 'type' not in report['scan']:
            raise RuntimeError(f'Failed to determine report type for project {self.name}')

        return report['scan']['type']

    def report_version(self):
        report = self.example_report_content()

        if 'version' not in report:
            return SchemaVersion('n/a')

        return SchemaVersion(report['version'])
