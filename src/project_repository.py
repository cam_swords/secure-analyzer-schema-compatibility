import json
from os import path

from .project import Project


class ProjectRepository:

    def __init__(self):
        self.projects_data = f'{path.dirname(path.abspath(__file__))}/../data/projects.json'

    def load(self, project):
        return Project(project['name'],
                       project.get('project_url', ''),
                       project.get('example_report', ''))

    def load_all(self):
        projects_data = json.loads(open(self.projects_data).read())
        return [self.load(project) for project in projects_data['projects']]
