from os import path


class ReadMe:

    def __init__(self):
        self.template = f'{path.dirname(path.abspath(__file__))}/templates/README.md.template'
        self.readme = f'{path.dirname(path.abspath(__file__))}/../README.md'

    def generate(self, compatibility_table):
        template_content = open(self.template).read()
        template_content = template_content.replace('%COMPATIBILITY_TABLE%', compatibility_table)
        open(self.readme, 'w').write(template_content)
