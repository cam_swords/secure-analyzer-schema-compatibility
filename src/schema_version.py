

class SchemaVersion:

    def __init__(self, schema_version):
        self.schema_version = schema_version

    def ordinal(self):
        return int(self.schema_version.replace('v', '').replace('-rc1', '').replace('.', ''))

    def __eq__(self, other):
        return self.ordinal() == other.ordinal()

    def __str__(self):
        return self.schema_version
