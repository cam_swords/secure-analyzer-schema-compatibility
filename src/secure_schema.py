from jsonschema import Draft4Validator

from .validation_result import ValidationResult


class SecureSchema:

    def __init__(self, schema, schema_version, schema_url):
        self.schema = schema
        self.schema_version = schema_version
        self.schema_url = schema_url

    def is_version(self, schema_version):
        return self.schema_version == schema_version

    def validate(self, to_verify):
        errors = Draft4Validator(self.schema).iter_errors(to_verify)
        return ValidationResult(list(errors), self)
