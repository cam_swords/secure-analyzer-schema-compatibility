
from .validation_results import ValidationResults


class SecureSchemas:

    def __init__(self, schemas):
        self.schemas = schemas

    def __len__(self):
        return len(self.schemas)

    def for_version(self, schema_version):
        for schema in self.schemas:
            if schema.is_version(schema_version):
                return schema

        return None

    def validate(self, content):
        results = [schema.validate(content) for schema in self.schemas]
        return ValidationResults(results)

    def most_recent(self):
        schemas = sorted(self.schemas, reverse=True,
                         key=lambda schema: schema.schema_version.ordinal())
        return SecureSchemas(schemas).first()

    def first(self):
        if len(self.schemas) == 0:
            raise RuntimeError('Failed to find first schema as there are none.')

        return self.schemas[0]
