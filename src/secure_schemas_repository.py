import json
from os import environ, makedirs, path

import requests

from .schema_version import SchemaVersion
from .secure_schema import SecureSchema
from .secure_schemas import SecureSchemas


class SecureSchemaRepository:

    def __init__(self):
        self.release_numbers = None
        self.schema_directory = f'{path.dirname(path.abspath(__file__))}/downloaded'
        self.project_url = 'https://gitlab.com/gitlab-org/security-products/security-report-schemas'

    def load(self, schema_version, project_type):
        makedirs(self.schema_directory, exist_ok=True)

        local_schema = f'{self.schema_directory}/{project_type}-schema-{schema_version}.json'
        url_project_type = project_type.replace('_', '-')
        schema_url = f'{self.project_url}/-/raw/{schema_version}/dist/{url_project_type}-report-format.json'

        if not path.exists(local_schema):
            response = requests.get(schema_url)

            if not response.ok:
                return None

            open(local_schema, 'wb').write(response.content)

        return SecureSchema(json.loads(open(local_schema).read()), SchemaVersion(schema_version), schema_url)

    def load_all(self, project_type):
        schemas = [self.load(release, project_type) for release in self.load_releases()]
        return SecureSchemas([schema for schema in schemas if schema])

    def load_releases(self):
        if self.release_numbers:
            return self.release_numbers

        headers = {'PRIVATE-TOKEN': environ.get('GITLAB_API_TOKEN', '')}
        response = requests.get('https://gitlab.com/api/v4/projects/16683102/releases', headers=headers)
        response.raise_for_status()

        self.release_numbers = [release['name'] for release in (json.loads(response.content))]
        return self.release_numbers
