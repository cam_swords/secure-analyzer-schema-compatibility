

class ValidationResult:

    def __init__(self, errors, schema):
        self.errors = errors
        self.schema = schema

    def schema_version(self):
        return self.schema.schema_version

    def schema_url(self):
        return self.schema.schema_url

    def succeeded(self):
        return self.number_of_violations() == 0

    def number_of_violations(self):
        return len(self.errors)

    def summarized_errors(self):
        summary = []

        for error in self.errors:
            path = '.'.join([str(path) for path in error.path])
            summary.append(f'{path}: {error.message}')

        return summary
