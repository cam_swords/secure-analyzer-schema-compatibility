
class ValidationResults:

    def __init__(self, validation_results):
        self.validation_results = validation_results

    def __len__(self):
        return len(self.validation_results)

    def least_errors(self):
        if len(self.validation_results) == 0:
            raise RuntimeError('Failed to find validation result with least errors as there are no results.')

        return self.by_most_recent_schema_version().sort_by_least_violations().first()

    def sort_by_least_violations(self):
        results = sorted(self.validation_results, key=lambda result: result.number_of_violations())
        return ValidationResults(results)

    def by_most_recent_schema_version(self):
        results = sorted(self.validation_results, reverse=True, key=lambda result: result.schema_version().ordinal())
        return ValidationResults(results)

    def first(self):
        if len(self.validation_results) == 0:
            raise RuntimeError('Failed to find first validation result as are no results.')

        return self.validation_results[0]
