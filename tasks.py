#!/usr/bin/env python

from invoke import Collection, task

from lib.tasks import compatibility, lint


@task(lint.python, default=True)
def default(context):
    pass


ns = Collection(default=default, lint=lint, compatibility=compatibility)
